Project Workflow and Conventions
=================
Base architecture for cemex projects.

## Specs
+ **Angular 4** 
+ **DLS (Design)** 
+ **.NET Core 2.0**

## Webpack includes
+ **SASS/SCSS support** 
+ **LESS support** 

## Setup:
1. Clone this repository (with submodules)
```sh
> git clone --recursive https://bitbucket.org/danielcardeenas/cemexbase.git
```
2. Inside project run compilation file
```sh
> cd src/cemex
> bash compile.sh
```

Thats it you are ready to go

## Screenshot

![Git Workflow](/sc.png?raw=true "Screenshot")