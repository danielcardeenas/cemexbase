import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component'
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

export const sharedConfig: NgModule = {
    bootstrap: [ AppComponent ],
    declarations: [
        // Host components
        AppComponent,
        DashboardComponent,

        // Content components
        HomeComponent
    ],
    imports: [
        RouterModule.forRoot([
            { path: '', redirectTo: 'app', pathMatch: 'full' },
            { path: 'home', redirectTo:'app', pathMatch:'full' },
            { path: 'app', component:  DashboardComponent, children: [
                { path: '', redirectTo: 'home', pathMatch:'full' },
                { path: 'home', component: HomeComponent },]
            },
            { path: '**', redirectTo: 'app' }
        ])
    ]
};
